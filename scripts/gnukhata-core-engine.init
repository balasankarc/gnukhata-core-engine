#!/bin/sh

# Copyright 2014 Praveen Arimbrathodityil <praveen@debian.org>
# Copyright 1998-2002, 2004, 2005 Branden Robinson <branden@debian.org>.
# Copyright 2006 Eugene Konev <ejka@imfi.kspu.ru>
#
# This is free software; you may redistribute it and/or modify
# it under the terms of the GNU General Public License as
# published by the Free Software Foundation; either version 2,
# or (at your option) any later version.
#
# This is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License with
# the Debian operating system, in /usr/share/common-licenses/GPL;  if
# not, write to the Free Software Foundation, Inc., 59 Temple Place,
# Suite 330, Boston, MA 02111-1307 USA

### BEGIN INIT INFO
# Provides:          gnukhata-core-engine
# Description:       Free Accounting Software
# Required-Start:    $local_fs $remote_fs postgresql
# Required-Stop:     $local_fs $remote_fs
# Should-Start:      
# Should-Stop:       
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Free Accounting Software
### END INIT INFO

set -e

PATH=/bin:/usr/bin:/sbin:/usr/sbin
DAEMON=/usr/bin/gkstart

. /usr/lib/gnukhata/scripts/init.sh

if [ -f ${CONFIG} ]
then
	USER=$(awk '/^user/{print $3}' "${CONFIG}")
	PIDFILE=$(awk '/^pidfile/{print $3}' "${CONFIG}")

	if [ "x${PIDFILE}" = "x" ]
	then
		echo "Cannot find pidfile in ${CONFIG}"
		exit 1
	fi
else
	echo "${CONFIG} not found"
	exit 1
fi

test -x $DAEMON || exit 0

. /lib/lsb/init-functions

SSD_START_ARGS="--pidfile $PIDFILE --chuid $USER --exec $DAEMON --startas $DAEMON"
SSD_STOP_ARGS="--pidfile $PIDFILE --chuid $USER --retry TERM/5/TERM/5"

case "$1" in
  start)
   # create run directory
   if ! [ -d /var/run/gnukhata ]; then
	install -d -m 2775 -o gnukhata /var/run/gnukhata
	[ -x /sbin/restorecon ] && restorecon -R /var/run/gnukhata || true
   fi

   # start gnukhata if not running already
   if ! status_of_proc -p $PIDFILE $DAEMON gnukhata-core-engine >/dev/null
   then
    log_daemon_msg "Starting GNUKhata core engine"
    start-stop-daemon --start --quiet $SSD_START_ARGS
   else
    log_daemon_msg "GNUKhata core engine is already running"
   fi 
   log_end_msg 0
  ;;

  restart)
    /etc/init.d/gnukhata-core-engine stop
    /etc/init.d/gnukhata-core-engine start
  ;;

  force-reload)
   rm -rf $PIDFILE
   if [ `ps ax |grep gkstart |wc -l` -eq 2 ] 
   then
    killall -9 gkstart
   fi
   [ -f $PIDFILE ] && exit 1
   /etc/init.d/gnukhata-core-engine start
  ;;

    stop)
     # stop gnukhata if running
     if status_of_proc -p $PIDFILE $DAEMON gnukhata-core-engine >/dev/null
     then
      log_daemon_msg "Stopping GNUKhata core engine"
      start-stop-daemon --stop --quiet $SSD_STOP_ARGS
      rm -rf $PIDFILE
     else
      log_daemon_msg "GNUKhata core engine is not running"
     fi
     log_end_msg 0
  ;;

  status)
	status_of_proc -p $PIDFILE $DAEMON gnukhata-core-engine && exit 0 || exit $?
  ;;

  *)
    echo "Usage: /etc/init.d/gnukhata-core-engine {start|stop|restart|force-reload|status}"
    exit 1
    ;;
esac

exit 0

# vim:set ai et sts=2 sw=2 tw=80:
